
## Features
- Native _HTML5_ video player
- Easy to use
- Play/Pause
- Seeking
- Volume
- Autoplay
- Preload
- Looping
- Scaling
- Fullscreen
- Download
- Buffering spinners
- Poster image
- Subtitles and text tracks
- Multiple media sources
- Customizable controls
- Material theming
- Keyboard shortcuts
- Fixed and responsive sizing
- Supports Chrome, Firefox, Safari, and Edge

## Installation
**mat-video** requires [Angular Material](https://material.angular.io/guide/getting-started) as a peer dependency, including animations.

```
npm install --save @angular/material @angular/cdk @angular/animations hammerjs
```

Add the following import to `src/polyfills.ts`:

```typescript
import 'hammerjs';
```

Add the following to your module:

```typescript
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatVideoModule } from 'mat-video';

@NgModule({
  imports: [
    BrowserAnimationsModule,
    MatVideoModule
  ],
})
export class AppModule { }
```

## Usage
A minimal example is quite simple, in your HTML file:

```html
    <mat-video src="localOrRemoteVideo.mp4"></mat-video>
```

A slightly more customized example, in your HTML file:

```html
    <mat-video title="My Tutorial Title" [autoplay]="true" [preload]="true" [fullscreen]="true" [download]="false" color="accent" spinner="spin" poster="image.jpg">
      <source matVideoSource src="tutorial.mp4" type="video/mp4">
      <source src="tutorial.webm" type="video/webm">
      <track matVideoTrack src="subtitles_en.vtt" kind="subtitles" srclang="en" label="English">
      <track src="subtitles_no.vtt" kind="subtitles" srclang="no" label="Norwegian">
    </mat-video>
```

## API
Attribute | Type | Description | Default
--- | --- | --- | ---
*src* | **string** | Path or URL to a video | *null*
*title* | **string** | Title for the video | *null*
*autoplay* | **boolean** | Whether the video should autoplay | *false*
*preload* | **boolean** | Whether the video should preload | *true*
*loop* | **boolean** | Whether the video should loop | *false*
*time* | **number** | Get or set the timestamp of the video | *0*
*muted* | **boolean** | Get or set whether the video is muted | *false*
*quality* | **boolean** | Whether the video will have a quality indicator | *true*
*download* | **boolean** | Whether the video will have a download option | *false*
*fullscreen* | **boolean** | Whether the video will have a fullscreen option | *true*
*showFrameByFrame* | **boolean** | Whether the video will display frame-by-frame controls | *false*
*keyboard* | **boolean** | Whether the player will have keyboard shortcuts | *true*
*overlay* | **boolean** | Force the overlay/controls to be shown or hidden | *null*
*color* | **ThemePalette** | Material theme color palette for the sliders | *primary*
*spinner* | **string** | Use 'spin', 'dot', 'split-ring', 'hourglass', or pass your own buffering spinner class | *spin*
*poster* | **string** | Path or URL to a poster image | *null*


The **_matVideoSource_** attribute can be used on the *source* tag to automatically reload the video when the source changes.

The **_matVideoTrack_** attribute can be used on the *track* tag to automatically reload the video when the track changes.

## Events
Listening to video events can be accomplished by directly accessing the video tag within **mat-video**.

In your HTML file:

```html
    <mat-video #video src="localOrRemoteVideo.mp4"></mat-video>
```

In your TS file:

```typescript
export class SampleComponent implements OnInit {
  @ViewChild('video') matVideo: MatVideoComponent;
  video: HTMLVideoElement;

  constructor(private renderer: Renderer2) { }

  ngOnInit(): void {
    this.video = this.matVideo.getVideoTag();

    // Use Angular renderer or addEventListener to listen for standard HTML5 video events
    
    this.renderer.listen(this.video, 'ended', () => console.log('video ended'));
    this.video.addEventListener('ended', () => console.log('video ended'));
  }
}
```


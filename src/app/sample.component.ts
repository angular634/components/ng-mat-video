import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './sample.component.html',
  styleUrls: ['./sample.component.css'],
})
export class SampleComponent implements OnInit {

  ngclass = 'mat-video-responsive';

  src = 'assets/NASA.mp4';
  title = 'NASA Rocket Launch';
  width = 'auto';
  height = 'auto';
  currentTime = 0;
  autoplay = false;
  preload = true;
  loop = false;
  quality = true;
  download = true;
  fullscreen = true;
  showFrameByFrame = false;
  keyboard = true;
  color = 'primary';
  spinner = 'spin';
  poster = 'assets/NASA.jpg';
  overlay = null;
  muted = false;
  constructor() {
  }
  ngOnInit(): void{
  }
}
